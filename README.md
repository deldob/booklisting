# booklisting

Dependencies: php

PHP program to create a webpage for your booklist, according to data from CSV.
CSV is used to be easily human-readable with a spreadsheet editor of your choice.

Separate your entries with semicolons `;` which allows to include commas in your entries.

Don't forget to adjust your spreadsheet program to avoid using commas as separators.

Name your files `books-<collectiontitle>.csv`. Headers will be made from that data.

then `php -S localhost:8000` to serve

To create a static HTML file, use `php index.php > index.html`

# TODOforthefuture

- make array that compiles all collections to make printing html elements independent from file parsing
- make bash script to auto create static html file
- use awk in bash to select and edit book entries
- add bash function to add new books and update reading progress

