<!DOCTYPE html>

<html>
<head>
<title>Biblidelyo</title>
<meta charset="UTF-8">
</head>

<?php $randhue=rand(0,360); echo "<body style='background-color:hsl({$randhue},55%,45%)'>" ?>
<?php

// Get all csv files
$files = glob("*.csv");

// Loop through each file
foreach ($files as $table){
	
	// Open the csv file to be read
	if (($open = fopen($table, "r")) !== false) {

		// Separate with semicolon and read each line
		while (($data = fgetcsv($open, 1000, ";")) !== false) {

			// Append it to the temporary array
        	$collection[] = $data;
    	}

		// Stop reading
    	fclose($open);

		// Add section based on collection name
		$title = strtoupper(str_replace(array('books-','.csv'), '', $table));
		
		echo "<h1>$title</h1>";
		

		echo "<p class='full'>";
		// Loop through temporay array as entries
		foreach ($collection as $book){

			// Random hue for css color
			$randhue = rand(0,360);

			// make span element from the book
			echo "<span class='item' style='background-color:hsl({$randhue},55%,45%)'>";
			
			// Loop through book properties and enclose them in inline links
			foreach	($book as $property){
				echo "<a class='property'>$property </a>";
			}

			// close the book entry
			echo "</span>";
		}
		
		echo "</p>";

		// clear temporary array
		$collection=[];
	}
}
?>



<p class="full">
These books/ <em>ces livres/</em> are in my/ <em>sont dans ma/</em> <strong>collection/</strong> and I can lend them/ <em>et je peux les prêter/</em> if you live in/ <em>si tu habites à/</em> <strong>Bru(x/ss)el(le)s.</strong>
<em>This page is made by reading a .csv spreadsheet file editable with excell, from a PHP file. It's the easiest way to setup a database if you're neither an expert nor do you have ten thousand items to show. Code upload soon.</em>
<br>
<a href="http://gitlab.com/deldob/booklisting">Download and customize</a>
</p>


<style>
@font-face{
font-family:"CMU Typewriter Regular";
src: url('fonts/CMU_Typewriter_Text_Regular.ttf');
font-weight: normal;
}
@font-face{
font-family:"CMU Typewriter Italic";
src: url('fonts/CMU_Typewriter_Text_Italic.ttf');
font-weight: normal;
font-style: italic;
}
@font-face{
font-family:"CMU Typewriter Bold";
src: url('fonts/CMU_Typewriter_Text_Bold.ttf');
font-weight: bold;
}
@font-face{
font-family:"CMU Typewriter Bold Italic";
src: url('fonts/CMU_Typewriter_Text_BoldItalic.ttf');
font-weight: bold;
font-style: italic;
}
* {margin: 0; padding: 0;}
.full, h1 {font-family:'CMU Typewriter Regular', serif; font-size: 24pt; color: black; line-height:normal;}
.item {color:white;}
.property {border-left: 2px dotted white; overflow-wrap:break-word; margin-right: 10px; padding-left: 10px;}
.item:hover {background-color:yellow; box-shadow: inset 0 0 5px 5px white;}
p em {background-color:rgba(255,255,255,0.2); font-family:'CMU Typewriter Italic'}
p strong {background-color:rgba(255,255,255,0.1); font-family:'CMU Typewriter Bold'}
</style>
<script>
</script>
</body>
</html>

